/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package syscall;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DOCENTE
 */
public class SysCall {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        String mensaje="LA FECHA ES:";
        //System.out.println(mensaje);
        //Syscall a través de java
        System.out.write(mensaje.getBytes(),0,mensaje.length());
        FileOutputStream salida=new FileOutputStream("src/datos/salida.txt");
        File fileName=new File("src/datos/salida.txt");
        PrintStream inicial=System.out;
        //Cambiar la salida estándar:
        System.setOut(new PrintStream(salida));
        System.out.write(mensaje.getBytes(),0,mensaje.length());
        System.out.write(mensaje.getBytes(),0,mensaje.length());
        System.out.write(mensaje.getBytes(),0,mensaje.length());
        //System.setOut(inicial);
        System.out.write(mensaje.getBytes(),0,mensaje.length());
               
        String comando[]={"notepad.exe",fileName.getAbsolutePath()};
        String comando2[]={"cmd.exe","/c","DATE /T"};

        Process proceso=Runtime.getRuntime().exec(comando2);
        
        Process proceso2=Runtime.getRuntime().exec(comando);
        
        
        BufferedReader is
                = new BufferedReader(new InputStreamReader(proceso.getInputStream()));
        String line;
        String msg="";
        // reading the output
        while ((line = is.readLine()) != null) {
            msg+=line+"\n";
            
        }
            System.out.println(msg);
        
        
        int codigo=1;
        try {
            codigo = proceso.waitFor();
        } catch (InterruptedException ex) {
            Logger.getLogger(SysCall.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Código:"+codigo);
        
         salida.close();

    }
    
}
